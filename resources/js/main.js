function saveAs() {
	"use strict";
	//    var layers = map.getLayers();
	var layer = jQuery.grep(map.getLayers().getArray(), function (layer) {
		console.log(layer.get('name') == 'Areas');
		return layer.get('name') == 'Areas';
	})[0];
	var format = new ol.format.GeoJSON();
	var obj = format.writeFeatures(layer.getSource().getFeatures());
	console.log(obj);


	uriContent = "data:application/octet-stream," + encodeURIComponent(obj);
	newWindow = window.open(uriContent, 'neuesDokument');

	//    download('test.txt', 'Hello world!');

}

function download(filename, text) {
	var pom = document.createElement('a');
	pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	pom.setAttribute('download', filename);

	if (document.createEvent) {
		var event = document.createEvent('MouseEvents');
		event.initEvent('click', true, true);
		pom.dispatchEvent(event);
	} else {
		pom.click();
	}
}

function featureArrayToCSV(featureArray) {
	console.log(featureArray);
	var CSV = "area,latitude,longitude,floor\n";
	featureArray.forEach(function (element) {
		var area = element.get('area')
		var coordinates = element.getGeometry().getCoordinates();
		var floor;
		if (element.get('floor')){
			floor = element.get('floor');
		}
		coordinates.forEach(function (element2) {
			element2.forEach(function (element3) {
				element3 = ol.proj.transform(element3, "EPSG:3857", "EPSG:4326");
				CSV += area + "," + element3[1] + "," + element3[0] + "," + floor + "\n";
			});
		});
	});
	return (CSV);
}

$("#CSV").on("click", function () {
	var layer = jQuery.grep(map.getLayers().getArray(), function (layer) {
		return layer.get('name') == 'Areas';
	})[0];
	var CSV = featureArrayToCSV(layer.getSource().getFeatures());
	console.log(CSV);
	$(this).attr("href", "data:text," + encodeURIComponent(CSV)).attr("download", "areas.csv");
});

// function of menu
$(function () {
	$("#menu").menu({
		items: "> :not(.ui-widget-header)"
	}).draggable();
});

$("#nav > li > a").on("click", function (e) {
	if ($(this).parent().has("ul")) {
		e.preventDefault();
	}

	if (!$(this).hasClass("open")) {
		// hide any open menus and remove all other classes
		//    $("#nav li ul").slideUp(350);
		//    $("#nav li a").removeClass("open");
		// open our new menu and add the open class
		$(this).next("ul").slideDown(350);
		$(this).addClass("open");
	} else if ($(this).hasClass("open")) {
		$(this).removeClass("open");
		$(this).next("ul").slideUp(350);
	}
});



function addList(name) {
	console.log(name);
	//  $("#menu").append('<li onclick="addList()">Option 7</li>');
}


//$("#GeoJSON").on("click", function() {
//  var layer = jQuery.grep(map.getLayers().getArray(), function (layer) {
//    return layer.get('name') == 'Areas';
//  })[0];
//  var featureArray = layer.getSource().getFeatures();
//  var outputFeatureArray;
//  featureArray.forEach( function (element) {
//    var outputFeature = new ol.Feature;
//    var coordinates = element.getGeometry().getCoordinates();
//    coordinates.forEach( function (element2) {
//      element2.forEach( function (element3) {   
//        console.log(element3);
//        element3 = ol.proj.transform(element3, "EPSG:3857", "EPSG:4326");
//        console.log(element3);
//      });
//    });
//  });
//  var format = new ol.format.GeoJSON();
//  var obj = format.writeFeatures(featureArray);
//  console.log(obj);
//  $(this).attr("href", "data:text," + encodeURIComponent(obj)).attr("download", "areas.geojson");
////  $(this).attr("href", "data:text/txt;utf-8," + CSV).attr("download", "areas.txt");
//});


var hiddenStyle = {
	'Point': [new ol.style.Style({
		image: new ol.style.Circle({
			radius: 0
		})
	})],
	'Polygon': [new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(0,255,255,0.5)'
		}),
		stroke: new ol.style.Stroke({
			color: '#0ff',
			width: 1
		})
	})]
}

var defaultStyle = {
	//  'Circle': [new ol.style.Style({
	//    stroke: new ol.style.Stroke({
	//        color: '#ff0',
	//        width: 1
	//      }),
	//    fill: new ol.style.Fill({
	//      color: 'rgba(235,235,20,0.5)'
	//    }),
	//  })],
	'Point': [new ol.style.Style({
		image: new ol.style.Circle({
			fill: new ol.style.Fill({
				color: 'rgba(235,235,20,0.5)'
			}),
			radius: 5,
			stroke: new ol.style.Stroke({
				color: '#ff0',
				width: 1
			})
		})
	})],
	'Polygon': [new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(0,255,255,0.1)'
		}),
		stroke: new ol.style.Stroke({
			color: '#0ff',
			width: 1
		})
	})]
}

var styleFunction = function (feature, resolution) {
	var featureStyleFunction = feature.getStyleFunction();
	if (featureStyleFunction) {
		return featureStyleFunction.call(feature, resolution);
	} else {
		return defaultStyle[feature.getGeometry().getType()];
	}
};

var dragAndDropInteraction = new ol.interaction.DragAndDrop({
	formatConstructors: [
                ol.format.GPX,
                ol.format.GeoJSON,
                ol.format.IGC,
                ol.format.KML,
                ol.format.TopoJSON
            ]
});

var areas = new ol.source.Vector({
	wrapX: false
});

var areasLayer = new ol.layer.Vector({
	source: areas,
	name: 'Areas',
	style: new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(255, 255, 255, 0.2)'
		}),
		stroke: new ol.style.Stroke({
			color: '#ffcc33',
			width: 3
		}),
		image: new ol.style.Circle({
			radius: 7,
			fill: new ol.style.Fill({
				color: '#ffcc33'
			})
		})
	})
});

//addCheckbox('Areas');


var map = new ol.Map({
	interactions: ol.interaction.defaults().extend([dragAndDropInteraction]),
	layers: [
        new ol.layer.Tile({
			source: new ol.source.OSM({
				//        layer: 'terrain'
			})
		}),      
    areasLayer
    ],
	target: 'map',
	view: new ol.View({
		//    center: ol.proj.fromLonLat([-122.39733476233808, 37.80156873280618]),
		center: ol.proj.fromLonLat([0, 0]),
		zoom: 2
	})
});

var addMapImage = function(file){
	var reader = new FileReader();
	var extent;
	var imgLoc;
	reader.readAsText(file, "UTF-8");
	reader.onload = function (evt) {
		var theseObjects = $.csv.toObjects(evt.target.result);	
		theseObjects.forEach(function (element, index, array) {
			imgLoc = element.imgLocation;
			extent = [
				parseFloat(element.longitudeW), 
				parseFloat(element.latitudeN), 
				parseFloat(element.longitudeE), 
				parseFloat(element.latitudeS)];
			extent = ol.extent.applyTransform(extent, ol.proj.getTransform("EPSG:4326", 	"EPSG:3857"));
			
			map.addLayer(new ol.layer.Image({
				source: new ol.source.ImageStatic({
					url: imgLoc,
					imageExtent: extent
				})
			}));
			zoomInToExtent(extent);
		})
	}
	spinner.stop();
}

var zoomInToExtent = function(extent){
	var zoom = ol.animation.zoom({
		resolution: map.getView().getResolution()
	});
	var pan = ol.animation.pan({
		source: map.getView().getCenter()
	});
	
	map.beforeRender(pan,zoom);
	console.log(extent);
	map.getView().fit(extent, map.getSize());
}


var element = document.getElementById('popup');

var popup = new ol.Overlay({
	element: element,
	positioning: 'bottom-center',
	stopEvent: false
});

map.addOverlay(popup);
var count = true;
map.on('click', function (evt) {
	if (!draw.getActive()) {
		if (count){
		var feature = map.forEachFeatureAtPixel(evt.pixel,
			function (feature, layer) {
				return feature;
			});
		if (feature) {
			popup.setPosition(evt.coordinate);
			$(element).popover({
				'placement': 'top',
				'html': true,
				'content': function () {
					var content = "";
					feature.getKeys().forEach(function (element) {
						if (element !== 'geometry') {
							content += (element + ": " + feature.get(element) + "<br>");
						}
					});
					content += "geometry: " + feature.getGeometry().getType();
					return content;
				}
			});
			$(element).popover('show');
		} else {
			popup.setPosition(evt.coordinate);
			$(element).popover({
				'placement': 'top',
				'html': true,
				'content': function(){
					var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326')
					return "lon: " + lonlat[0] + "<br>lat: " + lonlat[1]
				}
			});
			$(element).popover('show');
		}
		} else {
			count = true;
		}
	}
});

map.on('pointermove', function (e) {
	$(element).popover('destroy');
	//  if (e.dragging) {
	//    $(element).popover('destroy');
	//    return;
	//  }
	var pixel = map.getEventPixel(e.originalEvent);
	var hit = map.hasFeatureAtPixel(pixel);
	var cursorHoverStyle = "pointer";
	var target = map.getTarget();
	var jTarget = typeof target === "string" ? $("#" + target) : $(target);
	if (hit) {
		jTarget.css("cursor", cursorHoverStyle);
	} else {
		jTarget.css("cursor", "");
	}
});

// a drawing interaction for Polygons
var draw = new ol.interaction.Draw({
	source: areas,
	type: "Polygon",
});
draw.setActive(false);
var areaName; // make global so it can be added at the end of the draw
var floorNum;
function addArea() {
	bootbox.prompt("Enter Area Name (required)", function (result){
		if (result ===  null || result == ""){
				
		} else {
			bootbox.prompt("floor number (optional)", function (result2){
				if (result2 ===  null || result2 == ""){
				}else{
					floorNum = result2;
				}
			})
			areaName = result;	
			draw.setActive(true);
			count = false;
			map.addInteraction(draw);
		}
		
	})
//	areaName = prompt("Enter Area Name", "");
//	if (areaName ===  null || areaName == "")
//		return;
//	floorNum = prompt("Enter a floor number (optional)", "");	
	
}

draw.on('drawend', function (event) {
//	drawing = false;
	event.feature.setId(areaName);
	event.feature.set("area", areaName);
	if (floorNum){
		event.feature.set("floor", floorNum)
	}
	areas.addFeature(event.feature);
	addAreaCheckbox(areaName);
	map.removeInteraction(draw); 
	draw.setActive(false);
});

// add key listening to undo drawing if needed.
$(document).keydown(function(e) {
	if (draw.getActive()){
    if (e.keyCode == 27) { // escape key maps to keycode `27`
		map.removeInteraction(draw);
	}
	if (e.keyCode == 90 && e.metaKey || e.ctrlKey) {
			 draw.removeLastPoint();
    }
	}
});

// setup an array to hold the controller objects for each layer
var s;
var sourceControllerCollection = {
	settings: {
		array: [],
		duration: undefined,
	},
	init: function (sourceController) {
		s = this.settings;
		s.array.push(sourceController);
		this.setGlobals();
		this.initSlider();
	},
	setGlobals: function () {
		s.array.forEach(function (element) {
			if (typeof s.duration == 'undefined') {
				s.duration = element.duration;
			} else {
				s.duration = Math.max(s.duration, element.duration);
			}
		});
		console.log(s.duration);
	},
	initSlider: function () {
		$(function () {
			$("#slider-range").slider({
				range: true,
				min: 0,
				max: s.duration,
				values: [0, s.duration],
				slide: function (event, ui) {
					s.array.forEach(function (element) {
						element.handleSlide(ui.values);
					})
				}
			});
		});
	},
	addSource: function (source, name) {
		s.array.push(source);
		this.setGlobals();
		$("#slider-range").slider("option", "max", s.duration);
	}
}

function addAlphaChannel(color, alpha) {
	var alphaColor = color.replace('rgb', 'rgba').replace(')', ',' + alpha + ')'); //rgba(100,100,100,.8)
	return alphaColor;
}

// a controller object for handing each layer with one slider
function SourceController(source, name, color) {
	this.name = name;
	this.source = source;
	this.color = color;
	this.style = {
		'Point': [new ol.style.Style({
			image: new ol.style.Circle({
				fill: new ol.style.Fill({
					color: addAlphaChannel(this.color, .5)
				}),
				radius: 5,
				stroke: new ol.style.Stroke({
					color: 'rgb(235, 235, 235)',
					width: 1
				})
			})
		})]
	};
	//  console.log(this.style);
	var min, max;
	var style = this.style;
	source.getFeaturesCollection().forEach(function (feature, i, someArray) {
		feature.setStyle(style[feature.getGeometry().getType()]);
		var time = Number(feature.get('time'));
		feature.setId(time);
		if (typeof max == 'undefined') {
			max = time;
		} else {
			max = Math.max(max, time);
		}
		if (typeof min == 'undefined') {
			min = time;
		} else {
			min = Math.min(min, time);
		}
	});
	this.min = min;
	this.max = max;
	this.duration = max - min;
	this.leftPrevTime = min;
	this.rightPrevTime = max;
	this.arrayLength = source.getFeaturesCollection().getLength();
	this.leftIndex = 0;
	this.rightIndex = this.arrayLength - 1;
}

SourceController.prototype.handleSlide = function (values) {
	//take duration values and convert to time for this source
	var convertedValues = [this.min + values[0], this.min + values[1]];

	// cap the values for this source
	for (var i in convertedValues) {
		if (convertedValues[i] > this.max) {
			convertedValues[i] = this.max;
		}
	}

	var collection = this.source.getFeaturesCollection();
	//  left slider going up
	if (convertedValues[0] > this.leftPrevTime) {
		while (this.leftPrevTime < convertedValues[0]) {
			var feature = collection.item(this.leftIndex);
			feature.set("timeOut", true);
			feature.setStyle(hiddenStyle[feature.getGeometry().getType()]);
			this.leftPrevTime = collection.item(this.leftIndex).get('time');
			this.leftIndex++;
			if (this.leftIndex > this.arrayLength - 1) {
				this.leftIndex = this.arrayLength - 1
			};
		}
	}

	// left slider going down
	if (convertedValues[0] < this.leftPrevTime) {
		while (this.leftPrevTime > convertedValues[0]) {
			var feature = collection.item(this.leftIndex);
			feature.set("timeOut", false);
			//      feature.setStyle(defaultStyle[feature.getGeometry().getType()]);
			if (feature.get("visibility"))
				feature.setStyle(this.style[feature.getGeometry().getType()]);

			this.leftPrevTime = collection.item(this.leftIndex).get('time');
			this.leftIndex--;
			if (this.leftIndex < 0) {
				this.leftIndex = 0
			};
		}
	}

	// right slider going down
	if (convertedValues[1] < this.rightPrevTime) {
		while (this.rightPrevTime > convertedValues[1]) {
			var feature = collection.item(this.rightIndex);
			feature.set("timeOut", true);
			feature.setStyle(hiddenStyle[feature.getGeometry().getType()]);
			this.rightPrevTime = collection.item(this.rightIndex).get('time');
			this.rightIndex--;
			if (this.rightIndex < 0) {
				this.rightIndex = 0
			};
		}
	}

	// right slider going up
	if (convertedValues[1] > this.rightPrevTime) {
		while (this.rightPrevTime < convertedValues[1]) {
			var feature = collection.item(this.rightIndex);
			feature.set("timeOut", false);
			//      feature.setStyle(defaultStyle[feature.getGeometry().getType()]);
			if (feature.get("visibility"))
				feature.setStyle(this.style[feature.getGeometry().getType()]);

			this.rightPrevTime = collection.item(this.rightIndex).get('time');
			this.rightIndex++;
			if (this.rightIndex > this.arrayLength - 1) {
				this.rightIndex = this.arrayLength - 1
			};
		}
	}
}

function csvToFeatureCollection(file) {
	// double check if its a csv
	if (!file.name.endsWith(".csv")) {
		alert(file.name + " is not a .csv");
		return;
	} else {
		var reader = new FileReader();
		reader.readAsText(file, "UTF-8");
		reader.onload = function (evt) {
			var featureArray = [];
			var theseObjects = $.csv.toObjects(evt.target.result);
			var areaPolygon = [];
			theseObjects.forEach(function (element, index, array) {
				var geometry;
				var coordinates = [parseFloat(element.longitude), parseFloat(element.latitude)];
				coordinates = ol.proj.transform(coordinates, "EPSG:4326", "EPSG:3857"); // so they work on the map
				if (element.area) {
					if (array[index + 1] != null && element.area === array[index + 1].area) {
						areaPolygon.push(coordinates);
						return;
					} else if (element.area === array[index - 1].area) {
						geometry = new ol.geom.Polygon([areaPolygon]);
						areaPolygon = [];
					}
				} else {
					geometry = new ol.geom.Point(coordinates);
				}
				var feature = new ol.Feature({
					geometry: geometry
				});
				feature.setProperties(element);
				if (feature.getGeometry().getType() === 'Polygon') {
					feature.unset('longitude', true);
					feature.unset('latitude', true);
				}
				featureArray.push(feature);
			});
			addToMap(featureArray, file);
		}
	}
}

function addToMap(features, file) {
	name = file.name;
	//  addPropertiesMenu(features[0]);
	features[0].getKeys().forEach(function (element, index, array) {
			//    console.log(features[0].getProperties());

		})
		//  console.log(features[0].getKeys());
		//  console.log(features[0].getGeometry().getType() + " , " + features.length);
	if (features[0].getGeometry().getType() === 'Polygon') {
		var vectorSource = new ol.source.Vector({
			features: new ol.Collection(features),
			useSpatialIndex: true
		});
		
		var zoom = ol.animation.zoom({
			resolution: map.getView().getResolution()
		});
		var pan = ol.animation.pan({
			source: map.getView().getCenter()
		});
		map.beforeRender(pan,zoom);
		map.getView().fit(vectorSource.getExtent(), map.getSize());
		
		features.forEach(function (element, index, array) {
			var name = features[index].getProperties().area;
			element.setId(name);
		});
		areas.addFeatures(features);
		features.forEach(function (element, index, array){
			addAreaCheckbox(features[index].getProperties().area);		 
		});
		
	} else {
		features.forEach(function (element, index, array) {
			addPropertiesMenu(element);
			element.set("visibility", true);
			
			// this gets a little tricky we need to be more consistent with floor vs floorNum
			if (areas.getFeaturesAtCoordinate(element.getGeometry().getCoordinates()).length >0){
				if (element.get("floorNum")){
					areas.getFeaturesAtCoordinate(element.getGeometry().getCoordinates()).forEach( function(e) {
						if (e.get("floor")){
							if (element.get("floorNum") == e.get("floor")){
								element.set("xAreas", e.get("area"));
							}
						} else {
							// if there is no floor just set it to the last polygon
							element.set("xAreas", e.get("area"));
						}
					});
				} else {
					element.set("xAreas", areas.getFeaturesAtCoordinate(element.getGeometry().getCoordinates())[0].get("area"));
				}
			}
		});
	
	
		//create a vector source from the document
		var vectorSource = new ol.source.Vector({
			features: new ol.Collection(features),
			useSpatialIndex: true
		});
		
		//  vectorSource.getFeaturesCollection().forEach(function (feature, i, someArray) {
		//    console.log(feature.getGeometry());
		//    var coordinates = feature.getGeometry().getCoordinates();
		//    feature.setGeometry(new ol.geom.Circle(coordinates), 20);
		//    console.log(feature.getGeometry().getType());
		//  });

		// add it to the map
		map.getLayers().push(new ol.layer.Vector({
			source: vectorSource,
			name: name,
			style: styleFunction
		}));

		//    map.getView().fit(vectorSource.getExtent(),map.getSize());

		// if there a time attribute in the first feature of the vector source add a slider
		if ($.inArray('time', vectorSource.getFeatures()[0].getKeys()) > -1) {
			if (sourceControllerCollection.settings.array.length == 0) {
				sourceControllerCollection.init(new SourceController(vectorSource, name, getRandomRGBColor()));
			} else {
				sourceControllerCollection.addSource(new SourceController(vectorSource, name, getRandomRGBColor()));
			}
		}
		
		var zoom = ol.animation.zoom({
			resolution: map.getView().getResolution()
		});
		var pan = ol.animation.pan({
			source: map.getView().getCenter()
		});
		map.beforeRender(pan,zoom);
		map.getView().fit(vectorSource.getExtent(), map.getSize());	

		// add a checkbox for the layer
		addCheckbox(name);
	}
	updateElementsVisibility();
	spinner.stop();
}
var opts = {
  lines: 13 // The number of lines to draw
, length: 28 // The length of each line
, width: 14 // The line thickness
, radius: 42 // The radius of the inner circle
, scale: 1 // Scales overall size of the spinner
, corners: 1 // Corner roundness (0..1)
, color: '#fff' // #rgb or #rrggbb or array of colors
, opacity: 0.25 // Opacity of the lines
, rotate: 0 // The rotation offset
, direction: 1 // 1: clockwise, -1: counterclockwise
, speed: 1 // Rounds per second
, trail: 60 // Afterglow percentage
, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
, zIndex: 2e9 // The z-index (defaults to 2000000000)
, className: 'spinner' // The CSS class to assign to the spinner
, top: '50%' // Top position relative to parent
, left: '50%' // Left position relative to parent
, shadow: false // Whether to render a shadow
, hwaccel: false // Whether to use hardware acceleration
, position: 'absolute' // Element positioning
}

var spinnerTarget = document.getElementById('map')
var spinner = new Spinner(opts).spin(spinnerTarget)
spinner.stop();


dragAndDropInteraction.on('addfeatures', function (event) {
	spinner.spin(spinnerTarget);
	var name1 = event.file.name;
	
	// if it is the img text file handle it differently
	if (name1.endsWith("imgSettings.csv")){	
		addMapImage(event.file);		
		return;
	}
	
	//if it is a .csv file we need to handle that in another way
	if (name1.endsWith(".csv")) {
		csvToFeatureCollection(event.file);
		return;
	}
	
	// all other cases we jump to map adding function
	addToMap(event.features, event.file);

});


function getRandomHEXColor() {
	var letters = '0123456789ABCDEF'.split('');
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 10)];
	}
	return color;
}

function getRandomRGBColor() {
	var color = 'rgb(' + (Math.floor(Math.random() * 180)) + ',' +
		(Math.floor(Math.random() * 180)) + ',' +
		(Math.floor(Math.random() * 180)) + ')';
	return color;
}

var props = new ol.Collection; // to keep track of what properties have already been added to the list

var collection = new ol.Collection; // this keeps track of what attributes of a point should cause it to be hidden

function addPropertiesMenu(feature) {
	//  props.set("device", ["blue_01", "pear"]);
	var ul = document.getElementById('properties');
	feature.getKeys().forEach(function (element) {
		if (element != "time" && element != "geometry" && element != "longitude" && element != "latitude") {
			//			var collection;
			if (props.getKeys().some(function (el) {
					return element == el // returns true if the item is already listed
				})) {
				//				collection = params.get(element);
				//      console.log("already got: " + element);
				if (props.get(element).some(function (ela) {
						return feature.get(element) == ela; // returns true if the item value is already listed
					})) {
					//        console.log(element + " already has: " + feature.get(element));
				} else {
					collection.get(element).set(feature.get(element), true);
					//					collection.set("values", values);
					//        console.log(element + " need to add: " + feature.get(element));
					var ul2 = document.getElementById(element);
					var li2 = document.createElement('li');
					//					var li2 = ul2.lastChild.cloneNode(true);
					li2.id = feature.get(element);
					var label2 = document.createElement('label');
					var checkbox = document.createElement('input');
					checkbox.type = "checkbox";
					checkbox.checked = true;
					checkbox.name = feature.get(element);
					checkbox.value = "value";
					checkbox.id = feature.get(element);
					checkbox.onchange = function handleChange() {
						collection.get(element).set(feature.get(element), checkbox.checked);
						console.log(element + ", " + feature.get(element) + ": " + checkbox.checked);
						updateElementsVisibility();
					}
					label2.appendChild(document.createTextNode(feature.get(element)));
					li2.appendChild(checkbox);
					li2.appendChild(label2);
					ul2.appendChild(li2);

					var vals = props.get(element);
					vals.push(feature.get(element))
					props.set(element, vals);
				}
			} else {
				var values = new ol.Collection;
				values.set(feature.get(element), true);
				collection.set(element, values);
				//      console.log("need to add: " + element);
				props.set(element, [feature.get(element)])


				var liTemp = document.getElementById('empty');
				var li = liTemp.cloneNode(true);
				var label = document.createElement('label');
				label.appendChild(document.createTextNode(element));
				var ul2 = li.children[1];
				ul2.id = element;
				var li2 = document.createElement('li');
				li2.id = feature.get(element);
				var label2 = document.createElement('label');
				var checkbox = document.createElement('input');
				checkbox.type = "checkbox";
				checkbox.checked = true;
				checkbox.name = feature.get(element);
				checkbox.value = "value";
				checkbox.id = feature.get(element);
				checkbox.onchange = function handleChange() {
					collection.get(element).set(feature.get(element), checkbox.checked);
					console.log(element + ", " + feature.get(element) + ": " + checkbox.checked);
					updateElementsVisibility();
				}


				label2.appendChild(document.createTextNode(feature.get(element)));
				li2.appendChild(checkbox);
				li2.appendChild(label2);
				ul2.appendChild(li2);

				li.innerHTML = "";
				li.appendChild(label);
				li.appendChild(ul2);
				ul.appendChild(li);
			}
		}
	})
}

var updateElementsVisibility = function () {
	spinner.spin(spinnerTarget);
	sourceControllerCollection.settings.array.forEach(function (element, index, array) {
		element.source.getFeaturesCollection().forEach(function (feature, i, someArray) {
//			console.log(feature.get("xAreas"));
			feature.set("visibility", feature.getKeys().every(function (el) {
					var val = feature.get(el);
//					console.log(collection.get(el));
					if (collection.get(el)) {
						return collection.get(el).get(val);
					}
					return true;

				}))
				//			console.log(feature.get("visibility"));
			if (!feature.get("visibility") || feature.get("timeOut" )) {
				feature.setStyle(hiddenStyle[feature.getGeometry().getType()]);
			} else {
				feature.setStyle(element.style[feature.getGeometry().getType()]);
			}

		})
	})
	spinner.stop();
}

function addAreaCheckbox(checkName) {

		//make a new collection and add the area to it
		var aCollection = new ol.Collection
		aCollection.push(areas.getFeatureById(checkName))
			// make a new source and add the collection with only one area to it
		var vectorSource = new ol.source.Vector({
			features: aCollection,
			useSpatialIndex: true
		});
		
	
		var values = new ol.Collection;
		values.set(checkName, true);
		if(collection.get("xAreas")){
			collection.get("xAreas").set(checkName, true);
		} else {
			collection.set("xAreas", values);
		}
	
		// iterate over all the features in the area and add the polygon name as a feature
		sourceControllerCollection.settings.array.forEach(function (element, index, array) {
			element.source.getFeaturesCollection().forEach(function (feature, i, someArray) {
				if (vectorSource.getFeaturesAtCoordinate(feature.getGeometry().getCoordinates()).length > 0) {
					feature.set("xAreas", checkName);
				}
			});
		});

	var ul = document.getElementById('areas');
	var li = document.createElement('li');
	var checkbox = document.createElement('input');
	checkbox.type = "checkbox";
	checkbox.checked = true;
	checkbox.name = checkName;
	checkbox.value = "value";
	checkbox.id = checkName;
	checkbox.onchange = function handleChange() {
		console.log(checkName + ": " +checkbox.checked)
		collection.get("xAreas").set(checkName, checkbox.checked);
		updateElementsVisibility();
		
		
	};
	var label = document.createElement('label');
	label.appendChild(document.createTextNode(checkName));

	li.appendChild(checkbox);
	li.appendChild(label);
	ul.appendChild(li);
}

function addCheckbox(checkName) {
	var ul = document.getElementById('files');
	var li = document.createElement('li');
	var checkbox = document.createElement('input');
	checkbox.type = "checkbox";
	checkbox.checked = true;
	checkbox.name = checkName;
	checkbox.value = "value";
	checkbox.id = checkName;
	checkbox.onchange = function handleChange() {
		var layerName = this.name;
		var layer = jQuery.grep(map.getLayers().getArray(), function (layer) {
			return layer.get('name') == layerName;
		})[0];
		layer.setVisible(this.checked);
	};

	var label = document.createElement('label');
	//  label.htmlFor = "id";
	label.appendChild(document.createTextNode(checkName));

	li.appendChild(checkbox);
	li.appendChild(label);
	ul.appendChild(li);
}