function saveAs() {
  "use strict";
  //    var layers = map.getLayers();
  var layer = jQuery.grep(map.getLayers().getArray(), function (layer) {
    return layer.get('name') == "blue_js.json";
  })[0];
  var format = new ol.format.GeoJSON();
  var obj = format.writeFeatures(layer.getSource().getFeatures());
  console.log(obj);


  uriContent = "data:application/octet-stream," + encodeURIComponent(obj);
  newWindow = window.open(uriContent, 'neuesDokument');

  //    download('test.txt', 'Hello world!');

}

function download(filename, text) {
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  pom.setAttribute('download', filename);

  if (document.createEvent) {
    var event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  } else {
    pom.click();
  }
}






var hiddenStyle = {
  'Point': [new ol.style.Style({
    image: new ol.style.Circle({
      radius: 0
    })
  })],
  'Polygon': [new ol.style.Style({
    fill: new ol.style.Fill({
      color: 'rgba(0,255,255,0.5)'
    }),
    stroke: new ol.style.Stroke({
      color: '#0ff',
      width: 1
    })
  })]
}

var defaultStyle = {
  //  'Circle': [new ol.style.Style({
  //    stroke: new ol.style.Stroke({
  //        color: '#ff0',
  //        width: 1
  //      }),
  //    fill: new ol.style.Fill({
  //      color: 'rgba(235,235,20,0.5)'
  //    }),
  //  })],
  'Point': [new ol.style.Style({
    image: new ol.style.Circle({
      fill: new ol.style.Fill({
        color: 'rgba(235,235,20,0.5)'
      }),
      radius: 5,
      stroke: new ol.style.Stroke({
        color: '#ff0',
        width: 1
      })
    })
  })],
  'Polygon': [new ol.style.Style({
    fill: new ol.style.Fill({
      color: 'rgba(0,255,255,0.1)'
    }),
    stroke: new ol.style.Stroke({
      color: '#0ff',
      width: 1
    })
  })]
}

var styleFunction = function (feature, resolution) {
  var featureStyleFunction = feature.getStyleFunction();
  if (featureStyleFunction) {
    return featureStyleFunction.call(feature, resolution);
  } else {
    return defaultStyle[feature.getGeometry().getType()];
  }
};

var dragAndDropInteraction = new ol.interaction.DragAndDrop({
  formatConstructors: [
                ol.format.GPX,
                ol.format.GeoJSON,
                ol.format.IGC,
                ol.format.KML,
                ol.format.TopoJSON
            ]
});

var map = new ol.Map({
  interactions: ol.interaction.defaults().extend([dragAndDropInteraction]),
  layers: [
        new ol.layer.Tile({
      source: new ol.source.Stamen({
        layer: 'terrain'
      })
    }),
        new ol.layer.Image({
      source: new ol.source.ImageStatic({
        url: 'example-project/explo1.png',
        imageExtent: ol.extent.applyTransform([-122.398850, 37.802497, -122.396250, 37.800617],
          ol.proj.getTransform("EPSG:4326", "EPSG:3857"))
      })
    })
    ],
  target: 'map',
  view: new ol.View({
    center: ol.proj.fromLonLat([-122.39733476233808, 37.80156873280618]),
    zoom: 18
  })
});

// setup an array to hold the controller objects for each layer
var s;
var sourceControllerCollection = {
  settings: {
    array: [],
    duration: undefined,
  },
  init: function (sourceController) {
    s = this.settings;
    s.array.push(sourceController);
    this.setGlobals();
    this.initSlider();
  },
  setGlobals: function () {
    s.array.forEach(function (element) {
      if (typeof s.duration == 'undefined') {
        s.duration = element.duration;
      } else {
        s.duration = Math.max(s.duration, element.duration);
      }
    });
    console.log(s.duration);
  },
  initSlider: function () {
    $(function () {
      $("#slider-range").slider({
        range: true,
        min: 0,
        max: s.duration,
        values: [0, s.duration],
        slide: function (event, ui) {
          s.array.forEach(function (element) {
            element.handleSlide(ui.values);
          })
        }
      });
    });
  },
  addSource: function (source, name) {
    s.array.push(source);
    this.setGlobals();
    $("#slider-range").slider("option", "max", s.duration);
  }
}


// a controller object for handing each layer with one slider
function SourceController(source, name) {
  this.name = name;
  this.source = source;
  var min, max;
  source.getFeaturesCollection().forEach(function (feature, i, someArray) {
    var time = Number(feature.get('time'));
    feature.setId(time);
    if (typeof max == 'undefined') {
      max = time;
    } else {
      max = Math.max(max, time);
    }
    if (typeof min == 'undefined') {
      min = time;
    } else {
      min = Math.min(min, time);
    }
  });
  this.min = min;
  this.max = max;
  this.duration = max - min;
  this.leftPrevTime = min;
  this.rightPrevTime = max;
  this.arrayLength = source.getFeaturesCollection().getLength();
  this.leftIndex = 0;
  this.rightIndex = this.arrayLength - 1;
}

SourceController.prototype.handleSlide = function (values) {
  //take duration values and convert to time for this source
  var convertedValues = [this.min + values[0], this.min + values[1]];

  // cap the values for this source
  for (var i in convertedValues) {
    if (convertedValues[i] > this.max) {
      convertedValues[i] = this.max;
    }
  }

  var collection = this.source.getFeaturesCollection();
  //  left slider going up
  if (convertedValues[0] > this.leftPrevTime) {
    while (this.leftPrevTime < convertedValues[0]) {
      var feature = collection.item(this.leftIndex);
      feature.setStyle(hiddenStyle[feature.getGeometry().getType()]);
      this.leftPrevTime = collection.item(this.leftIndex).get('time');
      this.leftIndex++;
      if (this.leftIndex > this.arrayLength - 1) {
        this.leftIndex = this.arrayLength - 1
      };
    }
  }

  // left slider going down
  if (convertedValues[0] < this.leftPrevTime) {
    while (this.leftPrevTime > convertedValues[0]) {
      var feature = collection.item(this.leftIndex);
      feature.setStyle(defaultStyle[feature.getGeometry().getType()]);
      this.leftPrevTime = collection.item(this.leftIndex).get('time');
      this.leftIndex--;
      if (this.leftIndex < 0) {
        this.leftIndex = 0
      };
    }
  }

  // right slider going down
  if (convertedValues[1] < this.rightPrevTime) {
    while (this.rightPrevTime > convertedValues[1]) {
      var feature = collection.item(this.rightIndex);
      feature.setStyle(hiddenStyle[feature.getGeometry().getType()]);
      this.rightPrevTime = collection.item(this.rightIndex).get('time');
      this.rightIndex--;
      if (this.rightIndex < 0) {
        this.rightIndex = 0
      };
    }
  }

  // right slider going up
  if (convertedValues[1] > this.rightPrevTime) {
    while (this.rightPrevTime < convertedValues[1]) {
      var feature = collection.item(this.rightIndex);
      feature.setStyle(defaultStyle[feature.getGeometry().getType()]);
      this.rightPrevTime = collection.item(this.rightIndex).get('time');
      this.rightIndex++;
      if (this.rightIndex > this.arrayLength - 1) {
        this.rightIndex = this.arrayLength - 1
      };
    }
  }
}

function csvToFeatureCollection(file) {
  if (!file.name.endsWith(".csv")) {
    alert(file.name + " is not a .csv");
    return;
  } else {
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
      var featureArray = [];
      var theseObjects = $.csv.toObjects(evt.target.result);
      theseObjects.forEach(function (element, index, array) {

        var coordinates = [parseFloat(element.longitude), parseFloat(element.latitude)]
        var geometry = new ol.geom.Point(coordinates);
        var feature = new ol.Feature({
          geometry: geometry
        });
        feature.setProperties(element);
        featureArray.push(feature);
      });
      addToMap(featureArray, file);
    }
  }
}

function addToMap(features, file){
  name = file.name;
  console.log(features);
  features.forEach(function (element, index, array){
    console.log(element.getGeometryName());
  });
//  console.log(features.getGeometryName());
  //create a vector source from the document
  var vectorSource = new ol.source.Vector({
    features: new ol.Collection(features),
    useSpatialIndex: false
  });
  //  vectorSource.getFeaturesCollection().forEach(function (feature, i, someArray) {
  //    console.log(feature.getGeometry());
  //    var coordinates = feature.getGeometry().getCoordinates();
  //    feature.setGeometry(new ol.geom.Circle(coordinates), 20);
  //    console.log(feature.getGeometry().getType());
  //  });

  // add it to the map
  map.getLayers().push(new ol.layer.Vector({
    source: vectorSource,
    name: name,
    style: styleFunction
  }));


  // if there a time attribute in the first feature of the vector source add a slider
  if ($.inArray('time', vectorSource.getFeatures()[0].getKeys()) > -1) {
    console.log(sourceControllerCollection.settings.array);
    if (sourceControllerCollection.settings.array.length == 0) {
      sourceControllerCollection.init(new SourceController(vectorSource, name));
    } else {
      sourceControllerCollection.addSource(new SourceController(vectorSource, name));
    }
  }
  // add a checkbox for the layer
  addCheckbox(name);
}

dragAndDropInteraction.on('addfeatures', function (event) {
  var name = event.file.name;
  if (name.endsWith(".csv")) {
    csvToFeatureCollection(event.file);
    return;
  }
  
  addToMap(event.features, event.file);
});

function addCheckbox(checkName) {
  var newDiv = document.getElementById('uistuff');
  newDiv.style.position = "absolute";
  newDiv.style.right = "50px";
  newDiv.style.top = "50px";

  var checkbox = document.createElement('input');
  checkbox.type = "checkbox";
  checkbox.checked = true;
  checkbox.name = checkName;
  checkbox.value = "value";
  checkbox.id = checkName;
  checkbox.onchange = function handleChange() {
    var layerName = this.name;
    var layer = jQuery.grep(map.getLayers().getArray(), function (layer) {
      return layer.get('name') == layerName;
    })[0];
    layer.setVisible(this.checked);
  };

  var label = document.createElement('label');
  label.htmlFor = "id";
  label.appendChild(document.createTextNode(checkName));

  newDiv.appendChild(checkbox);
  newDiv.appendChild(label);
  document.body.appendChild(newDiv);
}