# IPS - TrackJS
Developed at the [Exploratorium](http://www.exploratorium.edu) in San Francisco, [*IPS-TrackJS*](https://bitbucket.org/exploratorium/ips_trackjs) and [*IPS-TrackR*](https://bitbucket.org/exploratorium/ips_trackr) are, respectively, open source Javascript and R projects used to view, explore and analyze geospatial track data within designated areas.

## Getting Started
For a quick intro to *IPS-TrackJS*, you can download this repository and play around with the example data.

- Click the `Downloads` link on the left side of this page (it looks like this: ![](ReadMeImgs/downloads.png))

- On the downloads page click the `Download repository` link.

- Unzip the downloaded file and find the `example` folder inside.

- Try it out by dragging one of the .csv data files into the [live demo](http://exploratorium.bitbucket.io/ips/).

## Basic Functions
*IPS-TrackJS* is pretty simple and provides some basic functionality to access your collected track data. It gives quick visual access to your data on a map and it provides a few tools to poke around and explore.

### Importing files
#### Drag and drop interaction

![](ReadMeImgs/import1.gif)

To get data into the visualizer, just drag the file from your computer on the map window. It will plot the points on the map using a random color for each individual track file. For area data it will draw the polygons on the map.

### Using the timeline

After you import some track data, a slider will appear at the bottom of the window. This slider controls the visibility of all points that have been imported based on their time from the beginning of their track.

#### Double-sided slider

![](ReadMeImgs/slider1.gif)

The double-sided slider at the bottom of the window allows you to adjust what timeframe of data is visible. Sliding the left handle to the right hides data starting from the beginning of the track. Sliding the right handle to the left hides data starting from the end of the track.

#### Time is zeroed for every file
In order to be able to compare different track files from different days, when a track is added to to the map, its time is zeroed to the earliest time in the file.

#### Keep time format consistent!
**WARNING**: seconds don't mix with milliseconds. If you are trying to visualize data with two different time scales, they will not play nice with the time slider. Whichever time frame you choose (milliseconds, seconds, minutes...) make sure it is consistent in each file you import

### Using the Menu

![](ReadMeImgs/menu1.gif)

The menu can be used to hide and show data that have specific attributes. 

- The `Areas` checkboxes specifically show/hide points that are located within the named area polygon

- The `Files` checkboxes show/hide the points from a spcified file.

- The `Properties` checkboxes show/hide points based on the keys and common values in the CSV or GeoJSON file that have been imported. More on this in the [Supported file formats](#markdown-header-supported-file-formats) section. (Note: this is very useful when using [*IPS-TrackR*](https://bitbucket.org/exploratorium/ips_trackr))

## How to create areas

You have already seen how to import an area. If you want to use the map to generate your own area polygons, here's how.

### Adding area polygons

![](ReadMeImgs/addarea.gif)

To add a new area manually, click on the `Add Area` button. This will start a polygon drawing tool and open a name dialog box. Give the area a name and an (optional) floor number. An area needs to have at least three points. To finish an area click on the first point. The area will be added to the areas layer of the map. 

### Undo a mistake when adding an area
We all make mistakes. If you happen to make one while you are drawing your new area polygon, you can press command+z(mac) or control+z(mac+windows) and it will undo the last point you clicked. Also, at any point before you finish drawing the polygon you can press 'esc' and it will exit out of the drawing tool and discard your unfinished polygon without adding it to the map. 

### Downloading Areas
In order to save the areas you have created on the map, click the *Download 'areas.csv'* link. This will download a file named 'areas.csv' containing all the area polygons currently on the map, including ones you have drawn and ones you have imported.

### Adding to an existing gallery file
If you want to add existing *areas.csv* file, import the file, then add areas, then click the *Download 'areas.csv'*. This link downloads all areas that have been added to the map.


## Supported file formats
This tool uses point data for tracks and polygon data for areas. You can import both CSV and geoJSON data formats.


#### CSV
CSVs have headers and must include: **latitude**, **longitude** and **time**.

It will look like this if you open the .csv file in a plain text editor:

```
latitude,longitude,time,floor,anything else
37.801222957884,-122.398433872615,1393910478731,will be
37.8011973719093,-122.398429577993,1393910478882,added to
37.8011953572315,-122.398424213444,1393910480013,properties
```

the .csv is a way to format a table of data to be easily parsed. This is what the file looks like in a table (if you open it with excel, or another spreadsheet program)

| latitude                 | longitude             | time                  | floorNum  | anything else    |
|--------------------------|-----------------------|-----------------------|-----------|------------------|
| 37.801222[]()957884      | -122.398433[]()872615 | 139391[]()047873[]()1 | 1         | will be          |
| 37.801197[]()371909[]()3 | -122.398429[]()577993 | 139391[]()047888[]()2 | 1         | added to         |
| 37.801195[]()357231[]()5 | -122.398424[]()213444 | 139391[]()048001[]()3 | 1         | properties       |


#### GeoJSON
GeoJSON is a specialized format for geo-spatial data. Like the CSV it must include the **latitude**, **longitude** and **time** for each point, but it holds **latitude**, **longitude** as an array in the **coordinates** key for the geometry feature with a property key set to **time**

```json
{
  "type":"FeatureCollection",
  "features":[{
    "type": "Feature",
    "geometry": {
      "type": "Point",
      "coordinates": [-122.398433872615,37.801222957884]
    },
    "properties": {
      "time":"1393910478731",
	  "floorNum":"1",
      "anything else":"will be"
    }
  },
  {
    "type": "Feature",
    "geometry": {
      "type": "Point",
      "coordinates": [-122.398429577993,37.8011973719093]
    },
    "properties": {
      "time":"1393910478882",
	  "floorNum":"1",
      "anything else":"added to"
    }
  },
  {
    "type": "Feature",
    "geometry": {
      "type": "Point",
      "coordinates": [-122.398424213444,37.8011953572315]
    },
    "properties": {
      "time":"1393910480013",
	  "floorNum":"1",
      "anything else":"properties"
    }
  }]
}
```


### Difference between points and polygons
The data in this ReadMe so far has been for point data. Points represent the individual moments in a track. Polygons are used to find out more information about the points. When a point is in a polygon, it is associated with that area, and can be shown or hidden along with the rest of the points in that area. More on this in the [Using the Menu](#markdown-header-using-the-menu) section.

Points, when imported, are grouped and colored (randomly) by file. Polygons, when added, are all grouped together to create a collection of areas. The areas in the visualization, whether they have been drawn or added from another can be downloaded as a collection of areas as a CSV. This file can be used with [IPS - TrackR](https://bitbucket.org/exploratorium/ips_trackr)

The data for Areas is a bit different because it is multiple points grouped to make a polygon.

#### Area CSV
CSVs for polygons also have headers and must include: **area**, **latitude** and **longitude**. **area** is the name of the polygon. It is also important that the points for each polygon be in order and that the first and last point of the polygon are the same.

It will look like this if you open the .csv file in a plain text editor:

```
area,longitude,latitude,floor
Entry,-122.398879,37.80101824,1
Entry,-122.3986395,37.80120818,1
Entry,-122.3979179,37.80158568,1
Entry,-122.3978875,37.8015382,1
Entry,-122.3979148,37.80151683,1
Entry,-122.3978966,37.80149783,1
Entry,-122.3980028,37.8014456,1
Entry,-122.3980179,37.8014551,1
Entry,-122.3984757,37.80120818,1
Entry,-122.3984757,37.80112033,1
Entry,-122.3983484,37.80097076,1
Entry,-122.3986728,37.80079032,1
Entry,-122.398879,37.80101824,1
West,-122.3984757,37.80121055,1
West,-122.3981422,37.80139099,1
West,-122.3978936,37.80111084,1
West,-122.3977693,37.80117969,1
West,-122.397742,37.80113695,1
West,-122.3981938,37.80087816,1
West,-122.3982999,37.80099687,1
West,-122.3983514,37.80097551,1
West,-122.3984788,37.80112271,1
West,-122.3984757,37.80121055,1

```


the .csv is a way to format the a table of data to be easily parsed. This is what the file looks like in a table (if you open it with excel, or another spreadsheet program). It has been shortened here, so as to not take up too much space in this ReadMe.

| area | latitude        | longitude      | floor  |
|------------------------|----------------|--------|
|Entry |-122.398879[]()  |37.801018[]()24 |1       |
|Entry |-122.398639[]()5 |37.801208[]()18 |1       |
|Entry | ...             |...             |...     |
|Entry |-122.398879[]()  |37.801018[]()24 |1       |
|West  |-122.398475[]()7 |37.801210[]()55 |1       |
|West  |-122.398142[]()2 |37.801390[]()99 |1       |
|West  | ...             |...             |...     |
|West  |-122.398475[]()7 |37.801210[]()55 |1       |

#### Areas GeoJSON
in GeoJSON format the latitude and logitude are in the **coordniates** key for each polygon. Similiar to the CSV the first and last coordinate of a polygon must be the same point. The **area** key is under the properties and holds the name of the area:

```json
{
  "type": "FeatureCollection",
  "features": [{
    "type": "Feature",
    "geometry": {
      "type": "Polygon",
      "coordinates": [
        [[-122.398879, 37.8010182428035],
         [-122.398639472, 37.8012081802253],
         [-122.397917856, 37.8015856808511],
         [-122.397887536, 37.8015381964956],
         [-122.397914824, 37.8015168285357],
         [-122.397896632, 37.8014978347935],
         [-122.398002752, 37.8014456020025],
         [-122.398017912, 37.8014550988736],
         [-122.398475744, 37.8012081802253],
         [-122.398475744, 37.8011203341677],
         [-122.3983484, 37.8009707584481],
         [-122.398672824, 37.8007903178974],
         [-122.398879, 37.8010182428035]]
      ]
    },
    "properties": {
      "area": "Entry",
	  "floor":"1"
    }
  },
  {
    "type": "Feature",
    "geometry": {
      "type": "Polygon",
      "coordinates": [
        [[-122.398475744, 37.8012105544431],
         [-122.398142224, 37.8013909949937],
         [-122.3978936, 37.8011108372966],
         [-122.397769288, 37.801179689612],
         [-122.397742, 37.8011369536921],
         [-122.398193768, 37.8008781639549],
         [-122.398299888, 37.8009968748436],
         [-122.398351432, 37.8009755068836],
         [-122.398478776, 37.8011227083855],
         [-122.398475744, 37.8012105544431]]
      ]
    },
    "properties": {
       "area": "West",
	   "floor":"1"
    }
  }]
}
```

## Relating to [*IPS-TrackR*](https://bitbucket.org/exploratorium/ips_trackr)
[*IPS-TrackR*](https://bitbucket.org/exploratorium/ips_trackr) is a repository of R example scripts and functions, which can be adapted and used for more in-depth data analyses.  Outputs from these R scripts can be 
 brought back into this tool for more data discovery.

## License
Unless stated otherwise, all content is licensed under the [Creative Commons Attribution License](http://creativecommons.org/licenses/by/3.0/) and code licensed under the [MIT License](http://creativecommons.org/licenses/MIT/).
Copyright (c) 2016 Doug Thistlewolf, Exploratorium

## Acknowledgement
This material is based upon work supported by the National Science Foundation under Grant No. 134666[]()4. Any opinions, findings, and conclusions or recommendations expressed in this material are those of the authors and do not necessarily reflect the views of the National Science Foundation.

![NSF Logo](ReadMeImgs/nsf1.jpg "NSF Logo")